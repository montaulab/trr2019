#ifndef MOTOR_HPP
#define MOTOR_HPP

#include <stdint.h>

#include "StateMachine.hpp"
#include "RampServo.hpp"

/* time of current loop */
extern unsigned long t;

class Motor
{
  public:
    static constexpr int8_t forwardMax = 20;
    static constexpr int8_t forwardSlow95 = 12;
    static constexpr int8_t reverseMax = -20;
    static constexpr int    servoDelay = 4;
    static constexpr int8_t kickAngle = 45;
    static constexpr unsigned long kickDuration = 50;
    static constexpr unsigned long BreakDuration = 1000;

    Motor()
      : mStateMachine( this )
    {
      mStateMachine.push( & Motor::stateStop );
    }

    inline void attach( int servoPin )
    {
      mRampServo.attach( servoPin );
    }

    inline void update()
    {
      mStateMachine.activate();
      mRampServo.update();
    }

    void setSpeed( int8_t speed )
    {
      if ( speed > forwardMax ) speed = forwardMax;
      if ( speed < reverseMax ) speed = reverseMax;
      mSpeed = speed;
    }

    inline void stop()
    {
      mSpeed = 0;
    }

    inline void forwardSlow()
    {
      mSpeed = 10;
    }

    inline void forwardFast()
    {
      mSpeed = forwardMax;
    }

    inline void reverseSlow()
    {
      mSpeed = -10;
    }

    inline void reverseFast()
    {
      mSpeed = reverseMax;
    }

  protected:
    void stateStop( bool entering )
    {
      if ( entering )
      {
        mRampServo.write( calibration.servoNeutral );
      }

      if ( mSpeed > 0 ) mStateMachine.set( & Motor::stateKickForward );
      if ( mSpeed < 0 ) mStateMachine.set( & Motor::stateKickReverse );
    }

    void stateKickForward( bool entering )
    {
      if ( entering )
      {
        mT = t + kickDuration;
        mRampServo.setAngle( calibration.servoNeutral + kickAngle, servoDelay );
      }

      if ( t >= mT ) mStateMachine.set( & Motor::stateForward );
    }

    void stateForward( bool )
    {
      if ( mSpeed <= 0 )
        mStateMachine.set( & Motor::stateBrake );
      else
        mRampServo.setAngle( calibration.servoNeutral + mSpeed, servoDelay );
    }

    void stateBrake( bool entering )
    {
      if ( entering )
      {
        mT = t + BreakDuration;
        mRampServo.setAngle( calibration.servoNeutral - kickAngle, servoDelay );
      }

      if ( t > mT ) mStateMachine.set( & Motor::stateStop );
    }

    void stateKickReverse( bool entering )
    {
      if ( entering )
      {
        mT = t + kickDuration;
        mRampServo.setAngle( calibration.servoNeutral - kickAngle, servoDelay );
      }

      if ( t >= mT ) mStateMachine.set( & Motor::stateReverse );
    }

    void stateReverse( bool )
    {
      if ( mSpeed >= 0 )
        mStateMachine.set( & Motor::stateStop );
      else
        mRampServo.setAngle( calibration.servoNeutral + mSpeed, servoDelay );
    }

  private:
    unsigned long mT = 0;
    StateMachine< Motor > mStateMachine;
    RampServo mRampServo;
    int8_t mSpeed = 0;

    struct Calibration {
      uint8_t servoNeutral = 90;
    } calibration;
};

#endif
