template< class T >
class StateMachine
{
  public:
    StateMachine( T * instance )
      : mInstance( instance )
    {}

    typedef void ( T::* StateFunction )( bool entering );

    bool push( StateFunction stateFunction )
    {
      if ( mStackSize >= mStackMax ) return false;

      ++mStackSize;
      mStack[ mStackSize ] = stateFunction;
      mChanging = true;
      return true;
    };

    bool pop()
    {
      if ( mStackSize < 0 ) return false;
      --mStackSize;
      return true;
    };

    inline bool set( StateFunction stateFunction )
    {
      pop();
      return push( stateFunction );
    }

    void activate()
    {
        if( mStackSize < 0 ) return;

        if( mChanging )
        {
            mChanging = false;
            ( mInstance->* mStack[ mStackSize ] ) ( true );
        }
        else
        {
            ( mInstance->* mStack[ mStackSize ] ) ( false );
        }
    }

  private:
    T * mInstance;
    static constexpr int mStackMax { 8 };
    StateFunction mStack[ mStackMax ] {};
    int mStackSize { -1 };
    bool mChanging = false;
};
