#include "Motor.hpp"

unsigned long t = 0;
unsigned long t0 = 0;

Motor motor;


bool timeAt( uint16_t timeStamp ) {
  return ( t0 <= timeStamp && t > timeStamp );
}

void reboot()
{
  asm volatile ("  jmp 0");
}

void setup()
{
  motor.attach( 9 );
}

void loop()
{
  t0 = t;
  t = millis();

  motor.update();

  if ( timeAt( 1000 ) ) motor.stop();

  if ( timeAt( 2000 ) ) motor.forwardSlow();

  if ( timeAt( 4000 ) ) motor.stop();

  if ( timeAt( 6000 ) ) motor.forwardFast();

  if ( timeAt( 8000 ) ) motor.stop();

  if ( timeAt( 10000 ) ) motor.reverseSlow();

  if ( timeAt( 12000 ) ) motor.stop();

  if ( timeAt( 14000 ) ) motor.reverseFast();

  if ( timeAt( 16000 ) ) motor.stop();
  
//  if ( timeAt( 18000 ) ) reboot();
}
