#include <Wire.h>

#include "Boussole.hpp"

Boussole boussole;

/* Variables globales
*/
// temps en millisecondes depuis le boot
unsigned long t = 0;
// temps en millisecondes de la derniere itération
unsigned long t0 = 0;

void setup()
{
  // laisse les périferiques démarrer
  delay( 100 );

  // configuration du port serie
  while ( ! Serial );
  Serial.begin( 57600 );

  // configuration du bus i2c
  Wire.begin();

  // configuration des composants
  boussole.init();

  boussole.loadAt( 1 );
}

void loop()
{
  // mise à jour du temps global
  t0 = t;
  t = millis();

  if ( ! boussole.update() ) return;

  float h = boussole.getHeading();

  Serial.print( h * 180.0 / PI + 180.0 );
  Serial.print( '\n' );
}
