#ifndef LIDAR_HPP
#define LIDAR_HPP

#include "TFmini.h"

class Lidar
{
  public:

    void init() {
      Serial2.begin( 115200 );

      tfmini.attach(Serial2);
      tfmini.setBaudRate( TFmini::Baudrate::BAUD_9600 );
    }

    void update() {
      while (tfmini.available())
      {
        mDistance = tfmini.getDistance();
      }
    }

    int16_t getDistance()
    {
      return mDistance;
    }
  private:
    TFmini tfmini;

    int16_t mDistance = 0;
};

#endif
