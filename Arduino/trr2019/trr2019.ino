#include <stdint.h>

#include "Direction.hpp"
#include "Motor.hpp"
#include "Boussole.hpp"
#include "Pilot.hpp"
#include "GPS.hpp"
#include "Lidar.hpp"

unsigned long t = 0;
unsigned long t0 = 0;

Direction direction;
Motor     motor;
Boussole  boussole;
GPS       gps;
Lidar     lidar;

Pilot pilot( direction, motor, boussole, gps, lidar );

void setup() {
  direction.attach( 8 );
  motor.attach( 9 );

  Serial.begin( 57600 );
  Serial.println( "setup()" );


  delay( 3000 );

  motor.update();
  direction.init();

  Wire.begin();

  boussole.init();
  boussole.loadAt( 1 );

  gps.init();

  lidar.init();

  pilot.init();

  Serial.println( "loop()" );
}

void loop() {
  t0 = t;
  t = millis();
/*
  gps.update();

  Serial.print(gps.getLat(), 6);
  Serial.print(F(","));
  Serial.print(gps.getLng(), 6);
  Serial.println();
  return;
*/

  pilot.update();
}
