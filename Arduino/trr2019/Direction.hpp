#ifndef DIRECTION_HPP
#define DIRECTION_HPP

#include <stdint.h>

#include "RampServo.hpp"

/* time of current loop */
extern unsigned long t;

class Direction
{
  public:
    static constexpr uint8_t steerMax    = 30;
    static constexpr int steerDelay      = 5;
    static constexpr int steerQuickDelay = 2;

    inline void attach( int servoPin )
    {
      mRampServo.attach( servoPin );
    }

    void init()
    {
      mRampServo.write( 45 );
    }

    inline void update()
    {
      mRampServo.update();
    }

    inline void turn( int8_t steer )
    {
      mRampServo.setAngle( calibration.servoNeutral - steer, steerDelay );
    }

    inline void turnQuick( int8_t steer )
    {
      mRampServo.setAngle( calibration.servoNeutral - steer, steerQuickDelay );
    }

    inline void turnRight( int8_t steer = steerMax ) {
      turn( steer );
    }
    inline void turnLeft( int8_t steer = steerMax ) {
      turn( -steer );
    }
    inline void turnRightQuick( int8_t steer = steerMax ) {
      turnQuick( steer );
    }
    inline void turnLeftQuick( int8_t steer = steerMax ) {
      turnQuick( -steer );
    }

    inline void goStraight(){
      turn( 0 );
    }

    inline void goStraightQuick(){
      turnQuick( 0 );
    }


  private:

    RampServo mRampServo;

    struct Calibration {
      uint8_t servoNeutral = 90;
    } calibration;

};

#endif
