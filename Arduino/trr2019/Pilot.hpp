#ifndef PILOT_HPP
#define PILOT_HPP


#include "StateMachine.hpp"
#include "Boussole.hpp"
#include "AverageFilter.hpp"
#include "Direction.hpp"
#include "Motor.hpp"
#include "GPS.hpp"
#include "Lidar.hpp"

struct Poi
{
  Poi() = default;

  Poi( double lat, double lng, double distance )
    : mLat( lat )
    , mLng( lng )
    , mDistance( distance )
  {}

  Poi( const Poi & rhs )
    : mLat( rhs.mLat )
    , mLng( rhs.mLng )
    , mDistance( rhs.mDistance )
  {}

  double mLat = 0.0;
  double mLng = 0.0;
  double mDistance = 0.0;
};

class Pilot
{
  public:
    Pilot( Direction & d, Motor & m, Boussole & b, GPS & g, Lidar & l )
      : mDirection( d )
      , mMotor( m )
      , mBoussole( b )
      , mGps( g )
      , mLidar( l )
      , mStateMachine( this )
    {
      mStateMachine.push( & Pilot::stateInitial );
    }

    void init()
    {
      pinMode( mStartPin, INPUT_PULLUP);
      pinMode( mEmergencyStopPin, INPUT_PULLUP);

      mMotor.update();

      for ( int i = 0; i < 10; ++i )
      {
        while ( ! mBoussole.update() )
        {
          t = millis();
        }

        Serial.print( "lecture " );
        Serial.println( mBoussole.getHeadingDegree() );
      }
    }

    void update()
    {
      if ( digitalRead( mEmergencyStopPin ) )
      {
        mStateMachine.set( & Pilot::stateDone );
        mStateMachine.activate();
      }

      mBoussole.update();
      mGps.update();
      mDirection.update();
      mMotor.update();
      mLidar.update();

      mStateMachine.activate();
    }

/*
    inline float getHeading()
    {
      return mAverageHeading.get();
    }
*/

  protected:

    void stateInitial( bool entry )
    {
      if ( entry )
      {
        mDirection.goStraight();
      }

      if ( ! mGps.isValid() )
      {
        Serial.println( "cap invalide" );
        return;
      }

      if ( digitalRead( mStartPin ) )
      {
        mStateMachine.set( & Pilot::stateStart );
      }
    }

    void stateStart( bool entry )
    {
      if ( entry )
      {
        mStartPoi.mLat = mGps.getLat();
        mStartPoi.mLng = mGps.getLng();
        mGps.setTarget( 43.59476, 1.450002 );
        mTargetHeading =  mGps.getHeading();
      }

      mMotor.forwardSlow();

      drive();

      if ( mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) >= 16 )
      {
        mStateMachine.set( & Pilot::stateRightTurn1 );
      }
    }

    void stateRightTurn1( bool entry )
    {
      if ( entry )
      {
        mStartPoi.mLat = mGps.getLat();
        mStartPoi.mLng = mGps.getLng();
        mGps.setTarget( 43.594696, 1.450042 );
        mTargetHeading =  mGps.getHeading();
      }

      drive();

      if ( mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) >= 2 )
      {
        mStateMachine.set( & Pilot::stateBack );
      }

    }

    void stateBack( bool entry )
    {
      if ( entry )
      {
        mStartPoi.mLat = mGps.getLat();
        mStartPoi.mLng = mGps.getLng();
        mGps.setTarget( 43.594359, 1.449462 );
        mTargetHeading =  mGps.getHeading();
      }

      drive();

      if ( mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) >= 45 )
      {
        mStateMachine.set( & Pilot::stateRightTurn2 );
      }
    }

    void stateRightTurn2( bool entry )
    {
      if ( entry )
      {
        mStartPoi.mLat = mGps.getLat();
        mStartPoi.mLng = mGps.getLng();
        mGps.setTarget( 43.594580, 1.444958 );
        mTargetHeading =  mGps.getHeading();
      }

      drive();

      if ( mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) >= 2 )
      {
        mStateMachine.set( & Pilot::stateLastStraight );
      }
    }

    void stateLastStraight( bool entry )
    {
      if ( entry )
      {
        mStartPoi.mLat = mGps.getLat();
        mStartPoi.mLng = mGps.getLng();
        mGps.setTarget( 43.59478, 1.449842 );
        mTargetHeading =  mGps.getHeading();
        ++count;
      }

      drive();

      if ( mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) >= 32 )
      {
        mStateMachine.set( & Pilot::stateDone );
      }
      /*
            if ( ( count >= 3 ) && mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) >= 32 )
            {
              mStateMachine.set( & Pilot::stateDone );
              return;
            }

            if ( mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) >= 47 )
            {
              mStateMachine.set( & Pilot::stateRightTurn1 );
            }
      */
    }

    /*
        void stateGoStrait( bool entry )
        {
          if ( entry )
          {
            mT = t + 30000;
            Serial.print( "Tout droit au " );
            Serial.print( mInitialHeading );

            mMotor.forwardSlow();
            //mMotor.forwardFast();
          }

          if ( t > mT )
          {
            mStateMachine.set( & Pilot::stateDone );
          }

          // corriger cap
          int16_t currentHeading = mBoussole.getHeadingDegree();
          int16_t targetHeading = mInitialHeading;

          while ( currentHeading > 180 )  currentHeading -= 360;
          while ( currentHeading < -180 ) currentHeading += 360;

          while ( targetHeading > 180 )  targetHeading -= 360;
          while ( targetHeading < -180 ) targetHeading += 360;

          int16_t diff = currentHeading - targetHeading;

          while ( diff > 180 )  diff -= 360;
          while ( diff < -180 ) diff += 360;

          mDirection.turn( -diff / 2 );

          if ( ( -6 < diff ) && ( diff < 6 ) )
          {
            if ( mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) < poiArray[ mPoiIndex ].mDistance / 2 )
            {
              mMotor.forwardFast();
            }
          }

          if ( mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) < poiArray[ mPoiIndex ].mDistance / 2 )
          {
            mInitialHeading = mGps.getHeading();
          }
          else
          {
            mMotor.forwardSlow();
          }

          if ( mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) >= poiArray[ mPoiIndex ].mDistance )
          {
            mStateMachine.set( & Pilot::stateUTurn );
          }
        }
    */

    /*
        void stateUTurn( bool  )
        {
          mStartPoi = poiArray[ mPoiIndex ];

          mPoiIndex = 1 - mPoiIndex;

          mGps.setTarget( poiArray[ mPoiIndex ].mLat, poiArray[ mPoiIndex ].mLng );

          mInitialHeading = mGps.getHeading();

          mMotor.forwardSlow();



          if ( count >= 6 )
          {
            mStateMachine.set( & Pilot::stateDone );
            return;
          }

          ++count;
          mStateMachine.set( & Pilot::stateGoStrait );

        }
    */

    void stateDone( bool  )
    {
      mMotor.stop();
    }

    void drive()
    {

      /*** Compass & GPS ***/
      int16_t compassAngle = 0;
      int16_t lidarAngle = 0;

      int16_t currentHeading = mBoussole.getHeadingDegree();
//      int16_t targetHeading =  mGps.getHeading();
      int16_t targetHeading = mTargetHeading;
      
      while ( currentHeading > 180 )  currentHeading -= 360;
      while ( currentHeading < -180 ) currentHeading += 360;

      while ( targetHeading > 180 )  targetHeading -= 360;
      while ( targetHeading < -180 ) targetHeading += 360;

      compassAngle = currentHeading - targetHeading;

      while ( compassAngle > 180 )  compassAngle -= 360;
      while ( compassAngle < -180 ) compassAngle += 360;

      compassAngle /= -2;

      /*** Lidar ***/
      lidarAngle = map( mLidar.getDistance(), 35, 50, -20, +20 );
      if ( lidarAngle < -20 ) lidarAngle = -20;
      if ( lidarAngle > 20 ) lidarAngle = 20;

      /*** MIX ***/
      //mDirection.turn( ( compassAngle + lidarAngle ) / 2 );

      mDirection.turn( compassAngle );

    }

  private:
    Direction & mDirection;
    Motor &     mMotor;
    Boussole &  mBoussole;
    GPS &       mGps;
    Lidar &     mLidar;

    int mStartPin = 7;
    int mEmergencyStopPin = 6;

    //AverageFilter< int16_t, 4 > mAverageHeading;

    StateMachine< Pilot > mStateMachine;

    Poi mStartPoi;

    unsigned long mT = 0;

    int count = 0;

    int16_t mTargetHeading = 0;

};

#endif
