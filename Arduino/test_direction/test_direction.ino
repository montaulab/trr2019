/*
  Controlling a servo position using a potentiometer (variable resistor)
  Speed
  Point mort 87.
  Arriere 84 à 75.
  Avant 90 à 100.
  penser a faire des rampes acceleration et freinage.

  direction
  point mort 95.
  droite 50
  gauche 170
*/
#include <stdint.h>

#include <Servo.h>

#include "Direction.hpp"

unsigned long t = 0;
unsigned long t0 = 0;

Direction direction;

bool timeAt( uint16_t timeStamp ) {
  return ( t0 <= timeStamp && t > timeStamp );
}

void reboot()
{
  asm volatile ("  jmp 0");
}

void setup() {
  direction.attach( 8 );
}

void loop() {
  t0 = t;
  t = millis();

  direction.update();

  if ( timeAt( 1000 ) ) direction.goStraight();

  if ( timeAt( 2000 ) ) direction.turnRight( Direction::steerMax );
  if ( timeAt( 3000 ) ) direction.goStraightQuick();

  if ( timeAt( 4000 ) ) direction.turnLeft( Direction::steerMax );
  if ( timeAt( 5000 ) ) direction.goStraightQuick();

  if ( timeAt( 6000 ) ) direction.turnRightQuick( Direction::steerMax );
  if ( timeAt( 7000 ) ) direction.goStraight();

  if ( timeAt( 8000 ) ) direction.turnLeftQuick( Direction::steerMax );
  if ( timeAt( 9000 ) ) direction.goStraight();

  if ( timeAt( 11000 ) ) reboot();
}
