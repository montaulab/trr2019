#ifndef RAMPSERVO_HPP
#define RAMPSERVO_HPP

#include <stdint.h>

#include <Servo.h>

/* time of current loop */
extern unsigned long t;

class RampServo
{
    static constexpr uint8_t defaultAngle = 90;
    static constexpr uint8_t maxAxgle     = 30;
    static constexpr int     defaultDelay = 10;
    static constexpr uint8_t limit        = 180;

  public:
    inline uint8_t getAngle() const
    {
      return mAngle;
    }

    inline setMin( uint8_t angle )
    {
      if ( angle > limit ) angle = limit;
      if ( angle > settings.max ) settings.max = angle;
      settings.min = angle;
    }

    inline setMax( uint8_t angle )
    {
      if ( angle > limit ) angle = limit;
      if ( angle < settings.min ) settings.min = angle;
      settings.max = angle;
    }

    void setAngle( uint8_t angle )
    {
      mSetpoint = angle;
      if ( mSetpoint > settings.max ) mSetpoint = settings.max;
      if ( mSetpoint < settings.min ) mSetpoint = settings.min;
    }

    inline void setAngle( uint8_t angle, unsigned long ms )
    {
      mDelay = ms;
      setAngle( angle );
    }

    inline unsigned long getDelay() const
    {
      return mDelay;
    }

    inline void setDelay( unsigned long ms )
    {
      mDelay = ms;
    }

    void update()
    {
      if ( t < mT ) return;

      if ( mAngle < mSetpoint ) {
        ++mAngle;
        mT = t + mDelay;
      }
      if ( mAngle > mSetpoint ) {
        --mAngle;
        mT = t + mDelay;
      }

      mServo.write( mAngle );
    }

    inline void attach( uint8_t pin ) {
      mServo.attach( pin );
    }

    inline void write( uint8_t angle ) {
      setAngle( angle );
    }

  private:
    Servo mServo;

    struct Settings {
      uint8_t min    = defaultAngle - maxAxgle;
      uint8_t max    = defaultAngle + maxAxgle;
      int     middle = defaultAngle;
      bool    swap   = false;
    } settings;

    uint8_t mAngle    = settings.middle;
    uint8_t mSetpoint = settings.middle;

    unsigned long mT = 0;
    unsigned long mDelay = defaultDelay;
};

#endif
