#include <Wire.h>

#include "Boussole.hpp"

Boussole boussole;

/* Variables globales
*/
// temps en millisecondes depuis le boot
unsigned long t = 0;
// temps en millisecondes de la derniere itération
unsigned long t0 = 0;

unsigned address = 1;

void setup()
{
  // laisse les périferiques démarrer
  delay( 100 );

  // configuration du port serie
  while ( ! Serial );
  Serial.begin( 57600 );

  // configuration du bus i2c
  Wire.begin();

  // configuration des composants
  boussole.init();

  Serial.print( "\nStatus boussole : " );
  Serial.println( boussole.getStatusString() );

}

void loop()
{
  t0 = t;
  do
  {
    t = millis();
  }
  while ( ! boussole.update() );

  if ( (t0 < 1000) && (t >= 1000) )
  {
    Serial.print( "Effectuez un tour complet sur les 3 axes\n" );
    Serial.print( "Sauvegarde dans 10s\n" );
  }

  if ( (t0 < 2000)  && (t >= 2000) )  Serial.print( "Sauvegarde dans 9s\n" );
  if ( (t0 < 3000)  && (t >= 3000) )  Serial.print( "Sauvegarde dans 8s\n" );
  if ( (t0 < 4000)  && (t >= 4000) )  Serial.print( "Sauvegarde dans 7s\n" );
  if ( (t0 < 5000)  && (t >= 5000) )  Serial.print( "Sauvegarde dans 6s\n" );
  if ( (t0 < 6000)  && (t >= 6000) )  Serial.print( "Sauvegarde dans 5s\n" );
  if ( (t0 < 7000)  && (t >= 7000) )  Serial.print( "Sauvegarde dans 4s\n" );
  if ( (t0 < 8000)  && (t >= 8000) )  Serial.print( "Sauvegarde dans 3s\n" );
  if ( (t0 < 9000)  && (t >= 9000) )  Serial.print( "Sauvegarde dans 2s\n" );
  if ( (t0 < 10000) && (t >= 10000) ) Serial.print( "Sauvegarde dans 1s\n" );
  if ( (t0 < 11000) && (t >= 11000) )
  {
    Serial.print( "Sauvegarde ..." );
    boussole.saveAt( address );
    Serial.print( " OK\n" );
  }

  if ( t >= 12000 ) calibrated();
}

void calibrated()
{
  while ( true )
  {
    t0 = t;
    t = millis();
    if ( ! boussole.update() ) continue;
    float h = boussole.getHeading();
    Serial.print( h * 180.0 / PI + 180.0 );
    Serial.print( '\n' );
  }
}
