#ifndef MOTEUR_HPP
#define MOTEUR_HPP

#include <stdint.h>

#include <Servo.h>

#include "configuration.hpp"

class Moteur
{
  public:
    void init()
    {
      setVitesse( moteurNeutre );
    }

    uint8_t getVitesse() const
    {
      return mVitesse;
    }
    void setVitesse( uint8_t vitesse )
    {
      mVitesse = constrain( vitesse, moteurVMaxAv, moteurVMaxAr );
    }

    void stop()
    {

    }

    void MAJ()
    {

    }

  private:
    bool freinActif = true;

    uint8_t mVitesse = moteurNeutre;
};

#endif
