#include "Boussole.hpp"
#include "Moteur.hpp"
#include "Direction.hpp"
#include <Wire.h>

Boussole boussole;
Moteur moteur;
Direction direction;

/* Variables globales
*/
// temps en millisecondes depuis le boot
unsigned long t = 0;
// temps en millisecondes de la derniere itération
unsigned long t0 = 0;

void etatBonjour();

typedef void (*Etat)();
Etat etatActuel = etatBonjour;
Etat etatPrecedent = etatBonjour;


void setup() {
  // configuration du port serie
  while ( ! Serial );
  Serial.begin( 57600 );

  // configuration du bus i2c
  Wire.begin();

  // configuration des composants
  boussole.init();
  moteur.init();
  direction.init();
}

void loop() {
  // mise à jour du temps global
  t0 = t;
  t = millis();

  boussole.lecture();

  Serial.println( boussole.getCap() );

  etatActuel();

  moteur.MAJ();
}

void etatBonjour()
{

}
