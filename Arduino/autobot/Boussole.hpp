#ifndef BOUSSOLE_HPP
#define BOUSSOLE_HPP

#include <stdint.h>

#include <Wire.h>

extern unsigned long t;

class Boussole
{
    static constexpr uint8_t HMC6352SlaveAddress = 0x42;
    static constexpr uint8_t HMC6352GetDataCmd   = 0x41;

    // delai entre la demande et le moment ou le cap est calculé
    static constexpr unsigned long HMC6352delay  = 6;

  public:
    void init()
    {
      mT = t;
      lecture();
      delay( HMC6352delay * 2 );
      lecture();
    }

    void lecture()
    {
      // delai passé ?
      if ( ( t - mT ) >= HMC6352delay )
      {
        // nouvelle mesure
        mT = t;
        Wire.beginTransmission( HMC6352SlaveAddress );
        Wire.write( HMC6352GetDataCmd ); // Get Data: Compensate and Calculate New Heading
        Wire.endTransmission();
      }
      else
      {
        // recupération du du cap demandé précédement
        Wire.requestFrom( HMC6352SlaveAddress, 2 );
        byte MSB = Wire.read();
        byte LSB = Wire.read();
        mCap = (float)( ( ( MSB << 8 ) + LSB ) / 10.0 );
      }
    }

    uint16_t getCap()
    {
      return mCap;
    }

  private:
    // temps de la dernière mesure
    unsigned long mT = 0;

    // dernier cap mesuré
    uint16_t mCap = 0;

};

#endif
