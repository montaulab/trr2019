#ifndef CONFIGURATION_HPP
#define CONFIGURATION_HPP

/* Moteur */
constexpr uint8_t moteurNeutre = 128;
constexpr uint8_t moteurVMinAv = 120;
constexpr uint8_t moteurVMaxAv = 90;
constexpr uint8_t moteurVMinAr = 140;
constexpr uint8_t moteurVMaxAr = 150;

#endif
