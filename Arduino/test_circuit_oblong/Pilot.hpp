#ifndef PILOT_HPP
#define PILOT_HPP


#include "StateMachine.hpp"
#include "Boussole.hpp"
#include "AverageFilter.hpp"
#include "Direction.hpp"
#include "Motor.hpp"
#include "GPS.hpp"

struct Poi
{
  Poi() = default;

  Poi( double lat, double lng, double distance )
    : mLat( lat )
    , mLng( lng )
    , mDistance( distance )
  {}

  Poi( const Poi & rhs )
    : mLat( rhs.mLat )
    , mLng( rhs.mLng )
    , mDistance( rhs.mDistance )
  {}

  double mLat = 0.0;
  double mLng = 0.0;
  double mDistance = 0.0;
};

Poi poiArray[ 2 ] = {
  Poi( 43.988674, 1.328633, 27.0 ),
  Poi( 43.988696, 1.328986, 27.0 )
};

class Pilot
{
  public:
    Pilot( Direction & d, Motor & m, Boussole & b, GPS & g )
      : mDirection( d ),
        mMotor( m ),
        mBoussole( b ),
        mGps( g ),
        mStateMachine( this )
    {
      mStateMachine.push( & Pilot::stateInitial );
    }

    void init()
    {
      mMotor.update();

      for ( int i = 0; i < 10; ++i )
      {
        while ( ! mBoussole.update() )
        {
          t = millis();
        }

        Serial.print( "lecture " );
        Serial.println( mBoussole.getHeadingDegree() );
      }


    }

    void update()
    {


      mBoussole.update();
      mGps.update();
      mDirection.update();
      mMotor.update();

      mStateMachine.activate();
    }

    inline float getHeading()
    {
      return mAverageHeading.get();
    }


  protected:

    void stateInitial( bool entry )
    {
      if ( entry )
      {
        mDirection.goStraight();
      }

      if ( ! mGps.isValid() )
      {
        Serial.println( "cap invalide" );
        return;
      }

      mGps.setTarget( poiArray[ 1 ].mLat, poiArray[ 1 ].mLng );

      mInitialHeading = mGps.getHeading();

      mStartPoi = poiArray[ 0 ];

      mStateMachine.set( & Pilot::stateGoStrait );

      Serial.print( "cap initial " );
      Serial.println( mInitialHeading );
    }

    void stateGoStrait( bool entry )
    {
      if ( entry )
      {
        mT = t + 30000;
        Serial.print( "Tout droit au " );
        Serial.print( mInitialHeading );

        mMotor.forwardSlow();
        //mMotor.forwardFast();
      }

      if ( t > mT )
      {
        mStateMachine.set( & Pilot::stateDone );
      }

      // corriger cap
      int16_t currentHeading = mBoussole.getHeadingDegree();
      int16_t targetHeading = mInitialHeading;

      while ( currentHeading > 180 )  currentHeading -= 360;
      while ( currentHeading < -180 ) currentHeading += 360;

      while ( targetHeading > 180 )  targetHeading -= 360;
      while ( targetHeading < -180 ) targetHeading += 360;

      int16_t diff = currentHeading - targetHeading;

      while ( diff > 180 )  diff -= 360;
      while ( diff < -180 ) diff += 360;

      mDirection.turn( -diff / 2 );

      if ( ( -6 < diff ) && ( diff < 6 ) )
      {
        if ( mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) < poiArray[ mPoiIndex ].mDistance / 2 )
        {
          mMotor.forwardFast();
        }
      }

      if ( mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) < poiArray[ mPoiIndex ].mDistance / 2 )
      {
        mInitialHeading = mGps.getHeading();
      }
      else
      {
        mMotor.forwardSlow();
      }

      if ( mGps.getDistance( mStartPoi.mLat, mStartPoi.mLng ) >= poiArray[ mPoiIndex ].mDistance )
      {
        mStateMachine.set( & Pilot::stateUTurn );
      }
    }

    void stateUTurn( bool  )
    {
      mStartPoi = poiArray[ mPoiIndex ];

      mPoiIndex = 1 - mPoiIndex;

      mGps.setTarget( poiArray[ mPoiIndex ].mLat, poiArray[ mPoiIndex ].mLng );

      mInitialHeading = mGps.getHeading();

      mMotor.forwardSlow();



      if ( count >= 6 )
      {
        mStateMachine.set( & Pilot::stateDone );
        return;
      }

      ++count;
      mStateMachine.set( & Pilot::stateGoStrait );

    }

    void stateDone( bool  )
    {
      mMotor.stop();
    }

  private:
    Direction & mDirection;
    Motor &     mMotor;
    Boussole &  mBoussole;
    GPS &       mGps;
    AverageFilter< int16_t, 4 > mAverageHeading;

    StateMachine< Pilot > mStateMachine;

    int16_t mInitialHeading = 0.0f;
    float mTargetHeading = 0.0f;

    Poi mStartPoi;

    unsigned long mT = 0;

    int count = 0;

    int mPoiIndex = 0;
};

#endif
