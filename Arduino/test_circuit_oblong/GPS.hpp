#ifndef GPS_HPP
#define GPS_HPP

#include <stdint.h>

#include <TinyGPS++.h>

/* time of current loop */
extern unsigned long t;

class GPS
{
  public:
    using Location = TinyGPSLocation;

    void init() {
      Serial1.begin( 9600 );
    }

    void update() {
      while ( Serial1.available() )
        gps.encode(Serial1.read());
      /*
            mPositionLat = gps.location.lat();
            mPositionLng = gps.location.lng();
      */
      mLocation = gps.location;
    }

    void setTarget( double lat, double lng )
    {
      mTargetLat = lat;
      mTargetLng = lng;
    }

    void setTarget( Location & location )
    {
      mTargetLat = location.lat();
      mTargetLng = location.lng();
    }

    bool isValid()
    {
      return mLocation.isValid();
    }

    int16_t getHeading()
    {
      return TinyGPSPlus::courseTo(
               mLocation.lat(),
               mLocation.lng(),
               mTargetLat,
               mTargetLng );
    }

    int16_t getHeading( Location & location )
    {
      return TinyGPSPlus::courseTo(
               mLocation.lat(),
               mLocation.lng(),
               location.lat(),
               location.lng() );
    }

    int16_t getDistance()
    {
      return TinyGPSPlus::distanceBetween(
               mLocation.lat(),
               mLocation.lng(),
               mTargetLat,
               mTargetLng );
    }

    int16_t getDistance( Location & location )
    {
      return TinyGPSPlus::distanceBetween(
               mLocation.lat(),
               mLocation.lng(),
               location.lat(),
               location.lng() );
    }

    
    int16_t getDistance( double lat, double lng )
    {
      return TinyGPSPlus::distanceBetween(
               mLocation.lat(),
               mLocation.lng(),
               lat,
               lng );
    }

    inline double getLat() const
    {
      return mPositionLat;
    }

    inline double getLng() const
    {
      return mPositionLng;
    }

    inline Location getLocation() const
    {
      return mLocation;
    }

  private:
    TinyGPSPlus gps;
    double mPositionLat;
    double mPositionLng;
    double mTargetLat;
    double mTargetLng;

    TinyGPSLocation mLocation;
};

#endif
