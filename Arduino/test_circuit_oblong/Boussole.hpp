#ifndef BOUSSOLE_HPP
#define BOUSSOLE_HPP

#include <stdint.h>

#include <Wire.h>
#include <EEPROM.h>

/* time of current loop */
extern unsigned long t;

/*
  http://www.magnetic-declination.com/
  Magnetic Declination: +0° 44'
  Declination is POSITIVE (EAST)
*/

class Boussole
{
    static constexpr byte address = 0x1E;
    static constexpr byte configurationRegistersIndex  = 0;
    static constexpr byte dataRegistersIndex           = 3;
    static constexpr byte identificationRegistersIndex = 10;
    static constexpr unsigned long timeout = 500;
    static constexpr int16_t offsetLimit   = 810;
    static constexpr float   deg2rad       = PI / 180.0;
    static constexpr float   rad2deg       = 180.0 / PI;
    static constexpr float   declinaison   = 44.0 / 60.0 * deg2rad;

    // struct matching configuration registers A & B
    union Configuration {
      // Values for number of samples averaged
      enum Average {
        avg_1 = 0b00,
        avg_2 = 0b01,
        avg_4 = 0b10,
        avg_8 = 0b11,
      };

      // Values for output rate
      enum OutputRate {
        f_0_75Hz = 0b000,
        f_1_5Hz  = 0b001,
        f_3Hz    = 0b010,
        f_7_5Hz  = 0b011,
        f_15Hz   = 0b100,
        f_30Hz   = 0b101,
        f_75Hz   = 0b110,
      };

      enum MeasurmentMode {
        normal       = 0b00,
        positiveBias = 0b01,
        negativeBias = 0b10,
      };

      enum Gain {
        gain_0_88 = 0b000,
        gain_1_3  = 0b001,
        gain_1_9  = 0b010,
        gain_2_5  = 0b011,
        gain_4_0  = 0b100,
        gain_4_7  = 0b101,
        gain_5_6  = 0b110,
        gain_8_1  = 0b111,
      };

      enum Mode {
        continuous = 0b00,
        single     = 0b01,
        idle       = 0b10,
      };

      struct {
        // Configuration register A
        uint8_t measurementMode : 2;
        uint8_t dataOutputRate  : 3;
        uint8_t measureAverage  : 2;
        uint8_t zeroA           : 1;  // Must be 0

        // Configuration register B
        uint8_t zeroB : 5;   // Must be 0
        uint8_t gain  : 3;

        // Mode register
        uint8_t operatingMode     : 2;
        uint8_t signleMeasureDone : 6; // These bits must be cleared for correct operation.
      };

      uint8_t raw[ 3 ];
    };

  public:
    union Data {
      struct {
        int16_t x;
        int16_t y;
        int16_t z;
      };

      uint8_t raw[ 6 ];
    };

    enum Status {
      ok,
      undefined,
      noResponse,
      badSignature,
    };

    void init() {
      checkDevice();
      readConfiguration();

      mConfiguration.measurementMode = Configuration::normal;
      mConfiguration.operatingMode   = Configuration::continuous;
      mConfiguration.measureAverage  = Configuration::avg_8;
      mConfiguration.dataOutputRate  = Configuration::f_15Hz;

      writeConfiguration();
    }

    inline Status getStatus() {
      return mStatus;
    }

    const char * getStatusString() const {
      switch ( mStatus )
      {
        case ok :           return "OK";
        case noResponse :   return "error device do not respond";
        case badSignature : return "error device have wrong signature";
        case undefined:
        default :           return "undefined";
      }
    }

    inline bool update() {
      return readData();
    }

    inline Data & getData() {
      return mData;
    }

    inline float getHeading() {
      return mHeading;
    }

    inline int16_t getHeadingDegree() {
      return mHeading * rad2deg + 180;
    }

    bool isReady() const {
      return ok == mStatus;
    }

    void loadAt( unsigned address ) {
      EEPROM.get( address, calibration );
    }

    void saveAt( unsigned address ) {
      EEPROM.put( address, calibration );
    }

  protected:
    bool checkDevice() {
      // check i2c link
      Wire.beginTransmission( address );
      if ( Wire.endTransmission() ) {
        mStatus = noResponse;
        return false;
      }

      // check signature
      Wire.beginTransmission( address );
      Wire.write( identificationRegistersIndex );
      Wire.endTransmission();
      Wire.requestFrom( (int)address, 3 );

      if ( ! waitBytes( 3 ) ) return false;

      mStatus = ok;
      byte x = 0;
      x = Wire.read();
      if ( x != 0b01001000 ) mStatus = badSignature;
      x = Wire.read();
      if ( x != 0b00110100 ) mStatus = badSignature;
      x = Wire.read();
      if ( x != 0b00110011 ) mStatus = badSignature;

      return isReady();
    }

    void readConfiguration() {
      Wire.beginTransmission( address );
      Wire.write( configurationRegistersIndex );
      Wire.endTransmission();
      Wire.requestFrom( (int)address, 3 );

      if ( ! waitBytes( 3 ) ) return;

      mConfiguration.raw[ 0 ] = Wire.read();
      mConfiguration.raw[ 1 ] = Wire.read();
      mConfiguration.raw[ 2 ] = Wire.read();

      mStatus = ok;
    }

    void writeConfiguration() {

      mConfiguration.zeroA = 0;
      mConfiguration.zeroB = 0;
      mConfiguration.signleMeasureDone = 0;

      Wire.beginTransmission( address );
      Wire.write( configurationRegistersIndex );
      Wire.write( mConfiguration.raw[ 0 ] );
      Wire.write( mConfiguration.raw[ 1 ] );
      Wire.write( mConfiguration.raw[ 2 ] );
      Wire.endTransmission();

      switch ( mConfiguration.dataOutputRate ) {
        case Configuration::f_0_75Hz : mDelay = 1337; break;
        case Configuration::f_1_5Hz  : mDelay = 667;  break;
        case Configuration::f_3Hz    : mDelay = 334;  break;
        case Configuration::f_7_5Hz  : mDelay = 134;  break;
        case Configuration::f_15Hz   : mDelay = 67;   break;
        case Configuration::f_30Hz   : mDelay = 34;   break;
        case Configuration::f_75Hz   : mDelay = 14;   break;
      }
    }

    bool waitBytes( uint8_t count ) {
      unsigned long limit = millis() + timeout;
      while ( Wire.available() < count ) {
        if ( millis() > limit ) {
          mStatus = noResponse;
          return false;
        }
      }
      return true;
    }

    bool readData() {
      switch ( mReadState ) {
        case request:
          Wire.beginTransmission( address );
          Wire.write( dataRegistersIndex );
          Wire.endTransmission();
          Wire.requestFrom( (int)address, 6 );
          mReadState = waitData;
          return false;
        case waitData:
          if ( Wire.available() < 6 ) return false;
          mData.raw[ 1 ] = Wire.read();
          mData.raw[ 0 ] = Wire.read();
          mData.raw[ 5 ] = Wire.read();
          mData.raw[ 4 ] = Wire.read();
          mData.raw[ 3 ] = Wire.read();
          mData.raw[ 2 ] = Wire.read();
          computeHeading();
          mT = t + mDelay;
          mReadState = waitDelay;
          return true;
        case waitDelay:
          if ( t > mT ) mReadState = request;
          return false;
        default :
          return false;
      }
    }

    void computeHeading() {
      if ( mData.x < calibration.xMin && mData.x > -offsetLimit ) {
        calibration.xMin = mData.x;
      }
      if ( mData.x > calibration.xMax && mData.x < offsetLimit ) {
        calibration.xMax = mData.x;
      }
      xOffset = ( calibration.xMin + calibration.xMax ) >> 1;

      if ( mData.y < calibration.yMin && mData.y > -offsetLimit ) {
        calibration.yMin = mData.y;
      }
      if ( mData.y > calibration.yMax && mData.y < offsetLimit ) {
        calibration.yMax = mData.y;
      }
      yOffset = ( calibration.yMin + calibration.yMax ) >> 1;

      mHeading = atan2f( mData.x - xOffset, mData.y - yOffset ) + declinaison;
    }

  private:
    Status mStatus = undefined;

    enum ReadState {
      request,
      waitData,
      waitDelay,
    };
    ReadState mReadState = request;

    Configuration mConfiguration = { .raw = {} };
    Data mData                   = { .raw = {} };

    struct Calibration {
    int16_t xMin = offsetLimit;
    int16_t xMax = -offsetLimit;
    int16_t yMin = offsetLimit;
    int16_t yMax = -offsetLimit;
    } calibration;
    
    int16_t xOffset = 0;
    int16_t yOffset = 0;

    unsigned long mDelay = 0;

    // temps de la dernière mesure
    unsigned long mT = 0;

    // dernier cap mesuré
    float mHeading = 0;
};

#endif
