#include <stdint.h>

#include "Direction.hpp"
#include "Motor.hpp"
#include "Boussole.hpp"
#include "Pilot.hpp"
#include "GPS.hpp"

unsigned long t = 0;
unsigned long t0 = 0;

Direction direction;
Motor     motor;
Boussole  boussole;
GPS       gps;

Pilot pilot( direction, motor, boussole, gps );

/*
  bool timeAt( uint16_t timeStamp )
  {
  return ( t0 <= timeStamp && t > timeStamp );
  }
*/

void setup() {
  Serial.begin( 57600 );
  while ( ! Serial );
  
  Serial.println( "setup()" );

  direction.attach( 8 );
  motor.attach( 9 );

  delay( 3000 );

  motor.update();
  direction.init();

  Wire.begin();

  boussole.init();
  boussole.loadAt( 1 );

  gps.init();

  pilot.init();

  gps.setTarget( 43.988903, 1.331675);

  Serial.println( "loop()" );
}

void loop() {
  t0 = t;
  t = millis();

  pilot.update();
}
