#ifndef AVERAGEFILTER_HPP
#define AVERAGEFILTER_HPP

#include <stdint.h>

template< typename T, unsigned int N >
class AverageFilter
{
public:
  void update( const T & newValue )
  {
    if( this->mValid < N ) ++this->mValid;
    this->mValues[ this->mIndex ] = newValue;
    ++this->mIndex;
    if( this->mIndex >= N ) this->mIndex = 0;
    this->mAverage = 0;
    for( uint8_t i = 0; i < this->mValid; ++i )
    {
      this->mAverage += this->mValues[ i ];
    }
    this->mAverage /= static_cast< T >( this->mValid );
  }

  inline const T & get() const
  {
    return this->mAverage;
  }

  inline bool isFilled() const
  {
    return ( mValid >= N );
  }
  
private:
  uint8_t mIndex = 0;
  uint8_t mValid = 0;
  T mValues[ N ];
  T mAverage = 0;
};

#endif
