#ifndef GPS_HPP
#define GPS_HPP

//#define SOFTWARE_SERIAL




class GPS
{
    static constexpr uint32_t GPSBaudRate = 9600;
    static constexpr int      bufferSize  = 64;

    static constexpr char     NemaHeader    = '$';
    static constexpr char     NemaSeparator = ',';

  public:

    enum Status {
      ok,
      undefined,
      noResponse,
    };

    enum NemaMessage {
      DTM,
      GBQ,
      GGA,
      GLL,
      GLQ,
      GNQ,
      GNS,
      GPQ,
      GRS,
      GSA,
      GST,
      GSV,
      RMC,
      TXT,
      VLW,
      VTG,
      ZDA,
    };


    void init() {
      mSerial.begin( GPSBaudRate );
      delay( 500 );
      /*
        checkDevice();
        readConfiguration();

        mConfiguration.measurementMode = Configuration::continuous;
        mConfiguration.dataOutputRate  = Configuration::f_15Hz;

        writeConfiguration();
      */
    }

    Status getStatus() {
      return mStatus;
    }

    const char * getStatusString() const {
      switch ( mStatus )
      {
        case ok :           return "OK";
        case noResponse :   return "error device do not respond";
        case undefined:
        default :           return "undefined";
      }
    }

    bool update() {
      return false;
    }

    bool disableSentence( NemaMessage );

    inline byte getChecksum() const {
      return mChecksum;
    }
    inline char getChecksumCharLow() const {
      return nibbleToChar( mChecksum & 0b00001111 );
    }
    inline char getChecksumCharHigh() const {
      return nibbleToChar( mChecksum >> 4 );
    }


  protected:
    bool readSentence() {

      switch ( mState ) {
      case waitHeader:
      break;
      case waitAddress:
      break;
      case parseGLL:
      break;
      case parseRMC:
      break;
      case waitChecksum:
      break;
      }

      return false;
    }

    void readGLL() {

    }

    void readRMC() {

    }

    char nibbleToChar( char c ) const {
      switch ( c & 0b00001111 )
      {
        case 0x0: return '0'; break;
        case 0x1: return '1'; break;
        case 0x2: return '2'; break;
        case 0x3: return '3'; break;
        case 0x4: return '4'; break;
        case 0x5: return '5'; break;
        case 0x6: return '6'; break;
        case 0x7: return '7'; break;
        case 0x8: return '8'; break;
        case 0x9: return '9'; break;
        case 0xa: return 'A'; break;
        case 0xb: return 'B'; break;
        case 0xc: return 'C'; break;
        case 0xd: return 'D'; break;
        case 0xe: return 'E'; break;
        case 0xf: return 'F'; break;
      }
    }

    inline void checksumAdd( char c ) {
      mChecksum ^= c;
    }
    inline void checksumClear() {
      mChecksum = 0;
    }

  private:

    enum State {
      waitHeader,
      waitAddress,
      parseGLL,
      parseRMC,
      waitChecksum,
    };
    enum SubState {
      waitLat,
      waitNS,
      waitLon,
      waitEW,
      waitTime,
    };
    
    Status mStatus = undefined;
    State mState = waitHeader;

    byte mChecksum = 0;
    char mBuffer[ bufferSize ] = "";
    int  mBufferIndex = 0;

#ifdef SOFTWARE_SERIAL
#include <SoftwareSerial.h>
    static constexpr int RXPin = 4, TXPin = 3;
    SoftwareSerial mSerial(RXPin, TXPin);
#else
#include <HardwareSerial.h>
    HardwareSerial & mSerial = Serial;
#endif
};

#endif
