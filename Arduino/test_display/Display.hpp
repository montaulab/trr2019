#ifndef BOUSSOLE_HPP
#define BOUSSOLE_HPP

#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Fonts/FreeMono9pt7b.h>

class Display
{
    void init()
    {
      display.begin( SSD1306_SWITCHCAPVCC, 0x3C );
      display.clearDisplay();
      display.setTextColor( WHITE );
    }

  private:
    Adafruit_SSD1306 mDisplay( 128, 64 );
};

#endif
