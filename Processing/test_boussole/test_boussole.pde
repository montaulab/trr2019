import processing.serial.*;

Serial myPort;  // The serial port
String serialDevice = "/dev/ttyACM0"; //default value
String serialSpeed  = "57600"; //default value
PFont font;

float angle = 0;

void setup() {
  myPort = new Serial(this, serialDevice, Integer.parseInt( serialSpeed ) );
  font = createFont( "Arial", 64, true );

  size( 800, 800 );
  fill( 0 );
  frameRate( 120 );
}

void draw() {
  if ( myPort.available() > 0 ) {
    String inBuffer = myPort.readStringUntil( '\n' );

    if ( inBuffer != null ) {
      try {
        angle = Float.parseFloat( inBuffer.trim() );
      } 
      catch(Exception e) {
      }
    }
  }

  translate( width / 2, height / 2 );

  background( 255 );
  stroke( 0 );
  strokeWeight( 4 );

  line( 0, 0, width / 3.0 * sin( angle * PI / 180.0 ), -height / 3.0 * cos( angle * PI / 180.0 ) );

  textFont( font, 64 );
  text( "N", -24, -height * 2.0 / 5.0 ) ;
  textFont( font, 32 );
  text( String.valueOf( round( angle ) ), -width / 2 + 5, -height / 2.0 + 30 ) ;
}
